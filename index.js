const secondE1 = document.querySelector(".second");
const minuteE1 = document.querySelector(".minute");
const hourE1 = document.querySelector(".hour");


function updateClock() {
    const currentDate = new Date();

    const second = currentDate.getSeconds();
    const minute = currentDate.getMinutes();
    const hour = currentDate.getHours();
    
    const secondDeg = (second / 60) * 360;
    secondE1.style.transform = `rotate(${secondDeg}deg)`;

    const minuteDeg = (minute / 60) * 360;
    minuteE1.style.transform = `rotate(${minuteDeg}deg)`;

    const hourDeg = (hour / 12) * 360;
    hourE1.style.transform = `rotate(${hourDeg}deg)`;
}

setInterval(updateClock, 1000);